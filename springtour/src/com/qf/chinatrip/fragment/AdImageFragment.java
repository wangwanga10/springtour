package com.qf.chinatrip.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.lidroid.xutils.BitmapUtils;
import com.qf.chinatrip.Constans;
import com.qf.chinatrip.R;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/24.
 * 9:39
 */
public class AdImageFragment extends Fragment implements View.OnClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_ad_image, container, false);

        ImageView imageView = (ImageView) ret.findViewById(R.id.img_ad_image);

        Bundle bundle = getArguments();

        String url = bundle.getString(Constans.ADVERTISIMENT_IMAGE_URL);

        System.out.println("url = " + url);

        BitmapUtils bitmapUtils = new BitmapUtils(getActivity());

        bitmapUtils.display(imageView, url);

        imageView.setOnClickListener(this);

        return ret;
    }

    @Override
    public void onClick(View v) {

    }
}