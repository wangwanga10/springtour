package com.qf.chinatrip.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.lidroid.xutils.BitmapUtils;
import com.qf.chinatrip.CommonWebViewActivity;
import com.qf.chinatrip.R;
import com.qf.chinatrip.model.ProductDetails;
import com.qf.chinatrip.utils.BitmapUtilsHelper;
import com.qf.chinatrip.utils.CommonUtils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Fcy on 2015/3/22.
 * “产品详情”的“基本信息”
 * “基本详情”的item数据：详情接口中的SubDict/Name（预订须知、供应标准、费用说明），根据个数及名字设置UI,对应ui的点击连接
 * Recommend 产品经理推荐内容
 */
public class DetailMsgFragment extends Fragment {

    private LinearLayout holder;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_detail_msg, container, false);
        context = getActivity();
        holder = (LinearLayout) ret.findViewById(R.id.details_msg_container);
        TextView textContent = (TextView) ret.findViewById(R.id.details_msg_content);
        ImageView headImg = (ImageView) ret.findViewById(R.id.details_msg_head);

        Bundle arguments = getArguments();
        if (arguments != null) {
            ProductDetails pro = (ProductDetails) arguments.getSerializable("product");
            if (pro != null) {
                // 推荐产品经理图像
                String managerImgUrl = pro.getRecommendManagerImgUrl();
                Log.i(">>", "详情信息Fragment:managerImgUrl=" + managerImgUrl);
                if (!TextUtils.isEmpty(managerImgUrl)) {
                    // 有头像，显示头像
                    headImg.setVisibility(View.VISIBLE);
                    // TODO 下载头像
                    BitmapUtils bitmapUtils = BitmapUtilsHelper.initUtils(context, R.drawable.ic_launcher);
                    bitmapUtils.display(headImg, managerImgUrl);
                }

                // 显示推荐内容
                textContent.setText(pro.getRecommend());

                List<LinkedHashMap<String, String>> subDict = pro.getSubDict();
                for (int i = 0; i < subDict.size(); i++) {
                    LinkedHashMap<String, String> map = subDict.get(i);
                    Log.i(">>", "map:" + map.toString());
                    String name = map.get("Name");
                    String url = map.get("Url");
                    // 动态添加视图
                    addItemView(name, url);
                }

                LinearLayout layout = new LinearLayout(context);
                layout.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, CommonUtils.dip2px(context, 55)));
                holder.addView(layout);
            }
        }

        return ret;
    }

    /**
     * 根据服务器返回的字段动态添加布局
     *
     * @param name 名字
     * @param url  跳转的url
     */
    public void addItemView(final String name, final String url) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_details_msg, null);

        TextView textName = (TextView) view.findViewById(R.id.details_msg_name);
        textName.setText(name);

        view.findViewById(R.id.details_msg_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 根据url 跳转
                CommonUtils.toast(getActivity(), url);
                Intent intent = new Intent(getActivity(), CommonWebViewActivity.class);
                intent.putExtra("title", name);
                intent.putExtra("webUrl", url);
                startActivity(intent);

            }
        });

        // 添加视图
        holder.addView(view);
    }
}