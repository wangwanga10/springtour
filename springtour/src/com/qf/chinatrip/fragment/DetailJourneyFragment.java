package com.qf.chinatrip.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.qf.chinatrip.R;

/**
 * Created by Fcy on 2015/3/24.
 */
public class DetailJourneyFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details_journey, container, false);
    }
}