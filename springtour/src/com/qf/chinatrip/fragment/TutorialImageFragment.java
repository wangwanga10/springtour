package com.qf.chinatrip.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.qf.chinatrip.Constans;
import com.qf.chinatrip.R;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 19:13
 */
public class TutorialImageFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getArguments();

        int layoutId = R.layout.fragment_tutorial_image1;

        if (bundle != null) {
            layoutId = bundle.getInt(Constans.ARG_TUTORIAL_IMAGE_LAYOUT_ID);
        }

        return inflater.inflate(layoutId, container, false);
    }
}