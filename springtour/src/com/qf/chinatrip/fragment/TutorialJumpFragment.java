package com.qf.chinatrip.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.qf.chinatrip.MainActivity;
import com.qf.chinatrip.R;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 19:17
 */
public class TutorialJumpFragment extends Fragment implements View.OnClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.fragment_tutorial_jump, container, false);

        Button btnStart = (Button) ret.findViewById(R.id.btn_tutorial_start);

        btnStart.setOnClickListener(this);

        return ret;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_tutorial_start){
            FragmentActivity activity = getActivity();
            startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
        }
    }
}