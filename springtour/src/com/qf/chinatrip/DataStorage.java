package com.qf.chinatrip;

/**
 * Created by Fcy on 2015/3/25.
 */
public class DataStorage {
    private static DataStorage ourInstance;

    public static DataStorage getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataStorage();
        }
        return ourInstance;
    }

    private DataStorage() {
    }
}
