package com.qf.chinatrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import com.qf.chinatrip.adapter.AlbumPagerAdapter;
import com.qf.chinatrip.adapter.CommonFragmentAdapter;
import com.qf.chinatrip.model.ProductDetails;
import com.qf.chinatrip.utils.BitmapUtilsHelper;

import java.util.*;

/**
 * Created by Fcy on 2015/3/23.
 * 点击“详情”中 图库进入的“影集”
 */
public class AlbumActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {

    private TextView albumInfo;
    private int imgCount = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        albumInfo = (TextView) findViewById(R.id.album_img_info);
        ViewPager viewPager = (ViewPager) findViewById(R.id.album_pager);
        viewPager.setOnPageChangeListener(this);


        Intent intent = getIntent();
        if (intent != null) {
            ProductDetails pro = (ProductDetails) intent.getSerializableExtra("product");
            if (pro != null) {
                List<String> imgUrls = new LinkedList<String>();
                List<LinkedHashMap<String, String>> productPics = pro.getProductPics();
                imgCount = productPics.size();
                for (int i = 0; i < productPics.size(); i++) {
                    String pic = productPics.get(i).get("Pic");
                    imgUrls.add(pic);
                }

                albumInfo.setText("1/" + imgCount);
                AlbumPagerAdapter adapter = new AlbumPagerAdapter(imgUrls, this);
                viewPager.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int position) {
        if (imgCount != -1) {
            albumInfo.setText(position + 1 + "/" + imgCount);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}