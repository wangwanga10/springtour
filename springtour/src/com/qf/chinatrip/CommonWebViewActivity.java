package com.qf.chinatrip;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import com.lidroid.xutils.BitmapUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by Fcy on 2015/3/23.
 * 必须传递 webUrl
 */
public class CommonWebViewActivity extends BaseActivity {

    private WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_webview);
        webView = (WebView) findViewById(R.id.common_webView);



        Intent intent = getIntent();
        if (intent != null) {
            // 设置标题
            String title = intent.getStringExtra("title");
            setTitle(title);

            String webUrl = intent.getStringExtra("webUrl");
            Log.i(">>", webUrl);
            try {
                String tarUrl = URLDecoder.decode(webUrl, "UTF-8");
                Log.i(">>", "解码:" + tarUrl);
                //http://m.springtour.com/Home/AppDetail/TextDetail?urlAddress=http://m.springtour.com/Home/NewTour/GetFeeNotice?routeId=13623
                webView.loadUrl(tarUrl);


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }
}