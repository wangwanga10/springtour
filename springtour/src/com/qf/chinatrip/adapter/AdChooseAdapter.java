package com.qf.chinatrip.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.qf.chinatrip.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/25.
 * 15:37
 */
public class AdChooseAdapter extends PagerAdapter {

    private Context context;
    private List<View> list;

    public AdChooseAdapter(Context context, List<View> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = list.get(position);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(list.get(position));
    }
}
