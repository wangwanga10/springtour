package com.qf.chinatrip;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qf.chinatrip.adapter.AdChooseAdapter;
import com.qf.chinatrip.adapter.CommonFragmentAdapter;
import com.qf.chinatrip.fragment.AdImageFragment;
import com.qf.chinatrip.model.Advertisement;
import com.qf.chinatrip.model.Category;
import com.qf.chinatrip.model.CategoryValue;
import com.qf.chinatrip.model.ProductItem;
import com.qf.chinatrip.utils.BitmapUtilsHelper;
import com.qf.chinatrip.utils.CommonUtils;
import com.qf.chinatrip.view.AutoScrollViewPager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ScrollView scrollView;

    private AutoScrollViewPager viewPagerAd;
    private RadioGroup radioGroup;
    private ViewPager viewPagerChoose;
    private RadioGroup radioGroupChoose;

    private ImageView imgMeiQi;
    private TextView textDangji;
    private TextView textMudi;

    private RelativeLayout layoutAdTop;
    private RelativeLayout layoutHotSell1;
    private ImageView imgHotSell1;
    private TextView textHotSell1;
    private ImageView imgHotSellCircle1;
    private TextView textHotSellType1;
    private TextView textHotSellPrice1;

    private RelativeLayout layoutHotSell2;
    private ImageView imgHotSell2;
    private TextView textHotSell2;
    private ImageView imgHotSellCircle2;
    private TextView textHotSellType2;
    private TextView textHotSellPrice2;

    private RelativeLayout layoutZhouTitle;
    private TextView textZhouTitle;
    private RelativeLayout layoutZhouAd;
    private ImageView imgZhou;
    private TextView textZhouName;
    private TextView textZhouPlace;
    private TextView textZhouPrice;
    private ImageView imgZhouLogo;

    private RelativeLayout layoutZi;
    private TextView textZiTitle;

    private RelativeLayout layoutZi1;
    private ImageView imgZiBg1;
    private TextView textZiName1;
    private TextView textZiPlace1;
    private TextView textZiPrice1;
    private ImageView imgZiLogo1;

    private RelativeLayout layoutZi2;
    private ImageView imgZiBg2;
    private TextView textZiName2;
    private TextView textZiPlace2;
    private TextView textZiPrice2;
    private ImageView imgZiLogo2;

    private TextView textActivityTitle;
    private ImageView imgActivity1;
    private ImageView imgActivity2;

    private List<Advertisement> advertisements;
    private CommonFragmentAdapter adAdapter;
    private LinkedList<Fragment> adFragments;
    private List<ProductItem> hotSellItems;
    private ProductItem zhouItesm;
    private List<ProductItem> ziyouItems;
    private List<Advertisement> activityItems;

    private ImageView imgZhoubian;
    private ImageView imgGuonei;
    private ImageView imgChujing;
    private ImageView imgAir;
    private ImageView imgMenpiao;
    private ImageView imgYoulun;
    private ImageView imgZijia;
    private Dialog dialog;
    private BitmapUtils bitmapUtils;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        dialog = CommonUtils.getProgressDialog(this, "正在加载...");
        bitmapUtils = BitmapUtilsHelper.initUtils(this, R.drawable.loading_normal);
//        bitmapUtils = BitmapUtilsHelper.initUtils(this);
//        bitmapUtils = new BitmapUtils(this);

        initFindId();
        advertisements = new LinkedList<Advertisement>();
        hotSellItems = new LinkedList<ProductItem>();
        zhouItesm = new ProductItem();
        ziyouItems = new LinkedList<ProductItem>();
        activityItems = new LinkedList<Advertisement>();

        adFragments = new LinkedList<Fragment>();
        adAdapter = new CommonFragmentAdapter(getSupportFragmentManager(), adFragments);
        viewPagerAd.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_CYCLE);
        viewPagerAd.setInterval(1000);
        viewPagerAd.setBorderAnimation(false);
        viewPagerAd.startAutoScroll(1000);
        viewPagerAd.setAdapter(adAdapter);
        viewPagerAd.setOnPageChangeListener(this);

        initAdChoose();

        TextView home = (TextView) findViewById(R.id.text_main_home);
        TextView find = (TextView) findViewById(R.id.text_main_find_find);
        TextView mine = (TextView) findViewById(R.id.text_main_mine_trip);
        TextView more = (TextView) findViewById(R.id.text_main_more);

        home.setOnClickListener(this);
        find.setOnClickListener(this);
        mine.setOnClickListener(this);
        more.setOnClickListener(this);

        imgMeiQi.setOnClickListener(this);
        textDangji.setOnClickListener(this);
        textMudi.setOnClickListener(this);
        layoutHotSell1.setOnClickListener(this);
        layoutHotSell2.setOnClickListener(this);
        layoutZhouTitle.setOnClickListener(this);
        layoutZhouAd.setOnClickListener(this);
        layoutZi.setOnClickListener(this);
        layoutZi1.setOnClickListener(this);
        if (layoutZi2.getVisibility() == View.VISIBLE){
            layoutZi2.setOnClickListener(this);
        }
        imgActivity1.setOnClickListener(this);
        imgActivity2.setOnClickListener(this);

        initHomeJson();
    }

    /**
     * 初始化七个旅游方式选择
     */
    private void initAdChoose() {
        List<View> list = new LinkedList<View>();
        LayoutInflater inflater = LayoutInflater.from(this);
        View ad1 = inflater.inflate(R.layout.item_ad_choose_1, null);
        View ad2 = inflater.inflate(R.layout.item_ad_choose_2, null);
        list.add(ad1);
        list.add(ad2);

        LinearLayout layoutZhoubian = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_zhoubian);
        LinearLayout layoutGuonei = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_guonei);
        LinearLayout layoutChujing = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_chujing);
        LinearLayout layoutAir = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_air);
        LinearLayout layoutMenpiao = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_menpiao);
        LinearLayout layoutYoulun = (LinearLayout) ad1.findViewById(R.id.layout_ad_choose_1_youlun);
        LinearLayout layoutZijia = (LinearLayout) ad2.findViewById(R.id.layout_ad_choose_2_zijia);

        imgZhoubian = (ImageView) ad1.findViewById(R.id.img_ad_choose_zhoubian);
        imgGuonei = (ImageView) ad1.findViewById(R.id.img_ad_choose_guonei);
        imgChujing = (ImageView) ad1.findViewById(R.id.img_ad_choose_chujing);
        imgAir = (ImageView) ad1.findViewById(R.id.img_ad_choose_air);
        imgMenpiao = (ImageView) ad1.findViewById(R.id.img_ad_choose_menpiao);
        imgYoulun = (ImageView) ad1.findViewById(R.id.img_ad_choose_youlun);
        imgZijia = (ImageView) ad2.findViewById(R.id.img_ad_choose_zijia);

        layoutZhoubian.setOnClickListener(this);
        layoutGuonei.setOnClickListener(this);
        layoutChujing.setOnClickListener(this);
        layoutAir.setOnClickListener(this);
        layoutMenpiao.setOnClickListener(this);
        layoutYoulun.setOnClickListener(this);
        layoutZijia.setOnClickListener(this);

        AdChooseAdapter adapter = new AdChooseAdapter(this, list);
        viewPagerChoose.setAdapter(adapter);
        RadioButton radioButton = (RadioButton) radioGroupChoose.getChildAt(0);
        radioButton.setChecked(true);
        radioButton.setClickable(false);
        viewPagerChoose.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                RadioButton radioButton = (RadioButton) radioGroupChoose.getChildAt(position);
                radioButton.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });
    }

    /**
     * 各种findViewById()
     */
    private void initFindId() {
        scrollView = (ScrollView) findViewById(R.id.scroll_main);

        layoutAdTop = (RelativeLayout) findViewById(R.id.layout_ad_top);
        // 服务器 海报图片大小 640 * 270
        int height = CommonUtils.getWindowsWidth(this) * 270 / 640;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, height);
        layoutAdTop.setLayoutParams(layoutParams);

        viewPagerAd = (AutoScrollViewPager) findViewById(R.id.view_pager_main_ad_top);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group_main_top_first);
        viewPagerChoose = (ViewPager) findViewById(R.id.view_pager_main_seven);
        radioGroupChoose = (RadioGroup) findViewById(R.id.radio_group_main_ad_choose);

        imgMeiQi = (ImageView) findViewById(R.id.img_main_meiqi);
        textDangji = (TextView) findViewById(R.id.text_main_dangji);
        textMudi = (TextView) findViewById(R.id.text_main_mudidi);

        layoutHotSell1 = (RelativeLayout) findViewById(R.id.layout_hot_sell_1);
        imgHotSell1 = (ImageView) findViewById(R.id.img_main_hot_1);
        textHotSell1 = (TextView) findViewById(R.id.text_main_hot_1);
        imgHotSellCircle1 = (ImageView) findViewById(R.id.img_main_hot_circle_1);
        textHotSellType1 = (TextView) findViewById(R.id.text_main_hot_type_1);
        textHotSellPrice1 = (TextView) findViewById(R.id.text_main_hot_price_1);

        layoutHotSell2 = (RelativeLayout) findViewById(R.id.layout_hot_sell_2);
        imgHotSell2 = (ImageView) findViewById(R.id.img_main_hot_2);
        textHotSell2 = (TextView) findViewById(R.id.text_main_hot_2);
        imgHotSellCircle2 = (ImageView) findViewById(R.id.img_main_hot_circle_2);
        textHotSellType2 = (TextView) findViewById(R.id.text_main_hot_type_2);
        textHotSellPrice2 = (TextView) findViewById(R.id.text_main_hot_price_2);

        layoutZhouTitle = (RelativeLayout) findViewById(R.id.layout_zhou_you);
        textZhouTitle = (TextView) findViewById(R.id.text_main_zhou_title);
        layoutZhouAd = (RelativeLayout) findViewById(R.id.layout_zhou_you_ad);
        imgZhou = (ImageView) findViewById(R.id.img_main_zhou);
        textZhouName = (TextView) findViewById(R.id.text_main_zhou_name);
        textZhouPlace = (TextView) findViewById(R.id.text_main_zhou_place);
        textZhouPrice = (TextView) findViewById(R.id.text_main_zhou_price);
        imgZhouLogo = (ImageView) findViewById(R.id.img_main_zhou_logo);

        layoutZi = (RelativeLayout) findViewById(R.id.layout_ziyou_xing);
        textZiTitle = (TextView) findViewById(R.id.text_main_ziyou_title);

        layoutZi1 = (RelativeLayout) findViewById(R.id.layout_ziyou_xing_1);
        imgZiBg1 = (ImageView) findViewById(R.id.img_main_ziyou_bg_1);
        textZiName1 = (TextView) findViewById(R.id.text_main_ziyou_name_1);
        textZiPlace1 = (TextView) findViewById(R.id.text_main_ziyou_place_1);
        textZiPrice1 = (TextView) findViewById(R.id.text_main_ziyou_price_1);
        imgZiLogo1 = (ImageView) findViewById(R.id.img_main_ziyou_logo_1);

        layoutZi2 = (RelativeLayout) findViewById(R.id.layout_ziyou_xing_2);
        imgZiBg2 = (ImageView) findViewById(R.id.img_main_ziyou_bg_2);
        textZiName2 = (TextView) findViewById(R.id.text_main_ziyou_name_2);
        textZiPlace2 = (TextView) findViewById(R.id.text_main_ziyou_place_2);
        textZiPrice2 = (TextView) findViewById(R.id.text_main_ziyou_price_2);
        imgZiLogo2 = (ImageView) findViewById(R.id.img_main_ziyou_logo_2);

        textActivityTitle = (TextView) findViewById(R.id.text_main_activity_title);
        imgActivity1 = (ImageView) findViewById(R.id.img_main_activity_1);
        imgActivity2 = (ImageView) findViewById(R.id.img_main_activity_2);
    }

    /**
     * 初始化顶部的广告动画
     */
    private void initViewPagerAdTop() {

        for (int i = 0; i < advertisements.size(); i++) {
            AdImageFragment adImageFragment = new AdImageFragment();
            Bundle args = new Bundle();
            Advertisement advertisement = advertisements.get(i);
            String imgSrc = advertisement.getImgSrc();
            args.putString(Constans.ADVERTISIMENT_IMAGE_URL, imgSrc);
            adImageFragment.setArguments(args);
            adFragments.add(adImageFragment);
        }
    }

    /**
     * 初始化正在热卖
     */
    private void initHotSell() {
        ProductItem productItem = hotSellItems.get(0);
        String productName = productItem.getProductName();
        String imgSrc = productItem.getImgSrc();
        String productTypeName = productItem.getProductTypeName();
        String title = productItem.getTitle();
        String price = productItem.getPrice();

        bitmapUtils.display(imgHotSell1, imgSrc, new BitmapLoadCallBack<ImageView>() {
            @Override
            public void onLoadCompleted(ImageView container,
                                        String uri, Bitmap bitmap,
                                        BitmapDisplayConfig config,
                                        BitmapLoadFrom from) {
                imgHotSell1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imgHotSell1.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadFailed(ImageView container, String uri, Drawable drawable) {

            }
        });
        textHotSell1.setText(title);
        int circleBgId = getCircleBg(productTypeName);
        imgHotSellCircle1.setImageResource(circleBgId);
        textHotSellType1.setText(productTypeName);
        textHotSellPrice1.setText("￥" + price + "起");

        if (hotSellItems.size() > 1){
            productItem = hotSellItems.get(1);
            productName = productItem.getProductName();
            imgSrc = productItem.getImgSrc();
            productTypeName = productItem.getProductTypeName();
            title = productItem.getTitle();
            price = productItem.getPrice();

            bitmapUtils.display(imgHotSell2, imgSrc, new BitmapLoadCallBack<ImageView>() {
                @Override
                public void onLoadCompleted(ImageView container,
                                            String uri,
                                            Bitmap bitmap,
                                            BitmapDisplayConfig config,
                                            BitmapLoadFrom from) {
                    imgHotSell2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imgHotSell2.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadFailed(ImageView container, String uri, Drawable drawable) {

                }
            });
            textHotSell2.setText(title);
            circleBgId = getCircleBg(productTypeName);
            imgHotSellCircle2.setImageResource(circleBgId);
            textHotSellType2.setText(productTypeName);
            textHotSellPrice2.setText("￥" + price + "起");
        }else {
            layoutHotSell2.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化周边游
     */
    private void initZhou() {
        String productName = zhouItesm.getProductName();
        String productTypeId = zhouItesm.getProductTypeId();
        String price = zhouItesm.getPrice();
        String destination = zhouItesm.getDestination();
        String imgSrc = zhouItesm.getImgSrc();

        bitmapUtils.display(imgZhou, imgSrc);
        int logoId = getLogoId(productTypeId);
        imgZhouLogo.setImageResource(logoId);
        textZhouName.setText(productName);
        textZhouPlace.setText(destination);
        textZhouPrice.setText("￥" + price);
    }

    /**
     * 初始化自由行
     */
    private void initZiyou() {
        ProductItem productItem = ziyouItems.get(0);
        String productName = productItem.getProductName();
        String imgSrc = productItem.getImgSrc();
        String destination = productItem.getDestination();
        String price = productItem.getPrice();
        String productTypeId = productItem.getProductTypeId();

        int logoId = getLogoId(productTypeId);
        imgZiLogo1.setImageResource(logoId);
        bitmapUtils.display(imgZiBg1, imgSrc);
        textZiName1.setText(productName);
        textZiPlace1.setText(destination);
        textZiPrice1.setText("￥" + price);

        if (ziyouItems.size() > 1){
            productItem = ziyouItems.get(1);
            productName = productItem.getProductName();
            imgSrc = productItem.getImgSrc();
            destination = productItem.getDestination();
            price = productItem.getPrice();
            productTypeId = productItem.getProductTypeId();

            bitmapUtils.display(imgZiBg2, imgSrc);
            textZiName2.setText(productName);
            textZiPlace2.setText(destination);
            textZiPrice2.setText("￥" + price);
            logoId = getLogoId(productTypeId);
            imgZiLogo2.setImageResource(logoId);
        }else {
            layoutZi2.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化活动
     */
    private void initActivityMent() {
        Advertisement advertisement = activityItems.get(0);
        String imgSrc = advertisement.getImgSrc();

        bitmapUtils.display(imgActivity1, imgSrc);

        if (activityItems.size() > 1){
            advertisement = activityItems.get(1);
            imgSrc = advertisement.getImgSrc();
            bitmapUtils.display(imgActivity2, imgSrc);
        }else {
            imgActivity2.setVisibility(View.GONE);
        }
    }

    /**
     * 根据类型名称获取圆圈背景
     * @param productTypeName
     * @return
     */
    private int getCircleBg(String productTypeName) {
        int ret = R.drawable.homepage_sell_begin_bg;
        if (productTypeName.equals("团队游")) {
            ret = R.drawable.homepage_sell_buging_bg;
        } else if (productTypeName.equals("自由行")) {
            ret = R.drawable.homepage_sell_begin_bg;
        } else {
            ret = R.drawable.homepage_sell_over_bg;
        }
        return ret;
    }

    /**
     * 根据类型Id获取Logo的Id
     * @param productTypeId
     * @return
     */
    private int getLogoId(String productTypeId) {
        int ret = R.drawable.packaged_dangdi_tag_ic;
        if (productTypeId.equals("7")){
            ret = R.drawable.packaged_dangdi_tag_ic;
        }else if (productTypeId.equals("2")){
            ret = R.drawable.packaged_tour_tag_ic;
        }else if (productTypeId.equals("5")){
            ret = R.drawable.packaged_ticket_tag_ic;
        }else {
            ret = R.drawable.packaged_zhoubian_tag_ic;
        }
        return ret;
    }

    /**
     * 点击事件处理
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent = null;
        switch (id) {
            case R.id.text_main_find_find:
                //TODO 底部的“找一找”
                intent = new Intent(this, FindActivity.class);
                break;
            case R.id.text_main_mine_trip:
                //TODO 底部的“我的春秋”
                intent = new Intent(this, MineActivity.class);
                break;
            case R.id.text_main_more:
                //TODO 底部的“更多”
                intent = new Intent(this, MoreActivity.class);
                break;
            case R.id.img_main_meiqi:
                //TODO 每期精选
                break;
            case R.id.text_main_dangji:
                //TODO 当季好去处
                break;
            case R.id.text_main_mudidi:
                //TODO 目的地
                break;
            case R.id.layout_hot_sell_1:
                //TODO 正在热卖1
                intent = new Intent(this, DetailsActivity.class);
                ProductItem productItem = hotSellItems.get(0);
                String productId = productItem.getProductId();
                intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                break;
            case R.id.layout_hot_sell_2:
                //TODO 正在热卖2
                intent = new Intent(this, DetailsActivity.class);
                productItem = hotSellItems.get(1);
                productId = productItem.getProductId();
                intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                break;
            case R.id.layout_zhou_you:
                //TODO 周边游标题栏
                break;
            case R.id.layout_zhou_you_ad:
                //TODO 周边游广告
                intent = new Intent(this, DetailsActivity.class);
                productId = zhouItesm.getProductId();
                intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                break;
            case R.id.layout_ziyou_xing:
                //TODO 自由行标题栏
                break;
            case R.id.layout_ziyou_xing_1:
                //TODO 自由行广告1
                intent = new Intent(this, DetailsActivity.class);
                productItem = ziyouItems.get(0);
                productId = productItem.getProductId();
                intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                break;
            case R.id.layout_ziyou_xing_2:
                //TODO 自由行广告2
                intent = new Intent(this, DetailsActivity.class);
                productItem = ziyouItems.get(1);
                productId = productItem.getProductId();
                intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                break;
            case R.id.img_main_activity_1:
                //TODO 活动1
                intent = new Intent(this, DetailsActivity.class);
                Advertisement advertisement = activityItems.get(0);
                Category catagory = advertisement.getCatagory();
                CategoryValue value = catagory.getValue();
                productId = value.getProductId();
                if (productId == null && !(productId.length() > 0)) {
                    String link = value.getLink();
                    intent.putExtra("Category_ID", link);
                }else {
                    intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                }
                break;
            case R.id.img_main_activity_2:
                //TODO 活动2
                intent = new Intent(this, DetailsActivity.class);
                advertisement = activityItems.get(1);
                catagory = advertisement.getCatagory();
                value = catagory.getValue();
                productId = value.getProductId();
                if (productId == null && !(productId.length() > 0)) {
                    String link = value.getLink();
                    intent.putExtra("Category_ID", link);
                }else {
                    intent.putExtra(Constans.KEY_INTENT_PRODUCT_ID, productId);
                }
                break;
            case R.id.layout_ad_choose_1_zhoubian:
                // TODO 周边游
                intent = new Intent(this, ZhouBianActivity.class);
                break;
            case R.id.layout_ad_choose_1_guonei:
                // TODO 国内游
                intent = new Intent(this, GuoneiActivity.class);
                break;
            case R.id.layout_ad_choose_1_chujing:
                // TODO 出境游
                intent = new Intent(this, ChujingActivity.class);
                break;
            case R.id.layout_ad_choose_1_air:
                // TODO 机票
                intent = new Intent(this, JipiaoActivity.class);
                break;
            case R.id.layout_ad_choose_1_menpiao:
                // TODO 门票
                intent = new Intent(this, MenpiaoActivity.class);
                break;
            case R.id.layout_ad_choose_1_youlun:
                // TODO 邮轮游
                intent = new Intent(this, YoulunActivity.class);
                break;
            case R.id.layout_ad_choose_2_zijia:
                // TODO 自驾游
                intent = new Intent(this, ZijiaActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    /**
     * 获取主页的Json数据
     */
    private void initHomeJson() {
        HttpUtils httpUtils = new HttpUtils();
        String url = Constans.HOME_URl;
        httpUtils.send(HttpRequest.HttpMethod.GET, url, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                String result = responseInfo.result;
                System.out.println("result = " + result);
                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String msg = jsonObject.getString("Msg");
                        if (msg.equals("Success")) {

                            scrollView.setVisibility(View.VISIBLE);
                            dialog.dismiss();

                            JSONObject response = jsonObject.getJSONObject("Response");
                            JSONArray advertisement = response.getJSONArray("Advertisement");
                            for (int i = 0; i < advertisement.length(); i++) {
                                RadioButton radioButton = new RadioButton(MainActivity.this);
                                radioButton.setBackgroundResource(R.drawable.selector_radio_main_ad_top);
                                radioButton.setButtonDrawable(new ColorDrawable());
                                radioButton.setClickable(false);
                                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(8, 8);
                                params.setMargins(3, 0, 3, 0);
                                radioButton.setLayoutParams(params);
                                radioGroup.addView(radioButton);

                                JSONObject adObject = advertisement.getJSONObject(i);
                                Advertisement ad = new Advertisement();
                                ad.parseJson(adObject);
                                advertisements.add(ad);
                            }
                            initViewPagerAdTop();

                            JSONObject timeLimitShopping = response.getJSONObject("TimeLimitShopping");
                            JSONArray timeLimitProductItem = timeLimitShopping.getJSONArray("ProductItem");
                            for (int i = 0; i < timeLimitProductItem.length(); i++) {
                                JSONObject object = timeLimitProductItem.getJSONObject(i);
                                ProductItem productItem = new ProductItem();
                                productItem.parseJson(object);
                                hotSellItems.add(productItem);
                            }
                            initHotSell();

                            JSONObject ambitusPlay = response.getJSONObject("AmbitusPlay");
                            String zhouOfiicial = ambitusPlay.getString("Ofiicial");
                            textZhouTitle.setText(zhouOfiicial);
                            JSONArray ambitusProductItem = ambitusPlay.getJSONArray("ProductItem");
                            JSONObject ambitusObject = ambitusProductItem.getJSONObject(0);
                            zhouItesm.parseJson(ambitusObject);
                            initZhou();

                            JSONObject freedomRide = response.getJSONObject("FreedomRide");
                            String freedomOfiicial = freedomRide.getString("Ofiicial");
                            textZiTitle.setText(freedomOfiicial);
                            JSONArray freedomProductItem = freedomRide.getJSONArray("ProductItem");
                            for (int i = 0; i < freedomProductItem.length(); i++) {
                                JSONObject object = freedomProductItem.getJSONObject(i);
                                ProductItem productItem = new ProductItem();
                                productItem.parseJson(object);
                                ziyouItems.add(productItem);
                            }
                            initZiyou();

                            JSONObject activityment = response.getJSONObject("Activityment");
                            String activityOfiicial = activityment.getString("Ofiicial");
                            textActivityTitle.setText(activityOfiicial);
                            JSONArray activity = activityment.getJSONArray("Activty");
                            for (int i = 0; i < activity.length(); i++) {
                                JSONObject object = activity.getJSONObject(i);
                                Advertisement ad = new Advertisement();
                                ad.parseJson(object);
                                activityItems.add(ad);
                            }
                            initActivityMent();
                            
                            adAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                System.out.println("s = " + s);
            }
        });
    }

    ///////////////////// ViewPager 滑动事件 ///////////////////////

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        RadioButton btn = (RadioButton) radioGroup.getChildAt(i);
        btn.setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
