package com.qf.chinatrip;

import android.app.Activity;
import android.os.Bundle;
import android.widget.BaseAdapter;

/**
 * Created by Fcy on 2015/3/20.
 */
public class MoreActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        setTitle("更多设置");
    }
}