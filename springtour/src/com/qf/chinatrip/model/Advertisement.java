package com.qf.chinatrip.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 20:00
 */
public class Advertisement {

    private String ImgSrc;// "http://media.china-sss.com/pics/6208abc4-f334-4ac8-a572-b7c11e0f298b_201502271921.jpg",
    private String Link;// "http://pages.springtour.com/Special/sj050227/index.html",
    private String Ofiicial;// null,
    private String BannerName;// "APP手机专享价尾单大促",
    private Category catagory;

    public void parseJson(JSONObject jsonObject) {
        try {
            ImgSrc = jsonObject.getString("ImgSrc");
            Link = jsonObject.getString("Link");//http://pages.springtour.com/Special/sj050227/index.html,
            Ofiicial = jsonObject.optString("Ofiicial");// null,
            BannerName = jsonObject.getString("BannerName");//APP手机专享价尾单大促,
            JSONObject object = jsonObject.getJSONObject("Category");
            catagory = new Category();
            catagory.parseJson(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getImgSrc() {
        return ImgSrc;
    }

    public String getLink() {
        return Link;
    }

    public String getOfiicial() {
        return Ofiicial;
    }

    public String getBannerName() {
        return BannerName;
    }

    public Category getCatagory() {
        return catagory;
    }
}