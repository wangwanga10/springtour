package com.qf.chinatrip.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/25.
 * 15:09
 */
public class Category {
    private String CategoryKey;
    private CategoryValue value;

    public void parseJson(JSONObject jsonObject) {
        try {
            CategoryKey = jsonObject.getString("CategoryKey");// "3",
            JSONObject object = jsonObject.getJSONObject("CategoryValue");
            value = new CategoryValue();
            value.parseJson(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getCategoryKey() {
        return CategoryKey;
    }

    public CategoryValue getValue() {
        return value;
    }
}
