package com.qf.chinatrip.model;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 19:49
 */

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 首页面中产品列表初步信息对象
 */
public class ProductItem {

    private String ProductName;// "闺蜜同游立减300元|普吉岛6日4晚自由行|入住梅尔加尔酒店(步行至海滩10分钟+赠送：接机服务)",
    private String ProductId;// "7158",
    private String Price;// "3199",
    private String SystemType;// "2",
    private String ProductType;// null,
    private String ImgSrc;// "http://media.china-sss.com/pics/1b43dfeb-e08b-4edf-864c-971b0ff50802_201502041546.jpg",
    private String ProductTypeName;// "自由行",
    private String ProductTypeId;// "2",
    private String BeginDate;// null,
    private String EndDate;// null,
    private String DateTimeNow;// "2015-03-22 20:28:37",
    private String BannerLink;// null,
    private String ProductState;// null,
    private String ProductSubName;// null,
    private String Title;// "闺蜜同游立减300元|普吉岛6日4晚自由行|入住梅尔加尔酒店(步行至海滩10分钟+赠送：接机服务)",
    private String Destination;// "普吉岛",
    private String Category;// null

    public void parseJson(JSONObject jsonObject){
        try {
            ProductName = jsonObject.getString("ProductName");// "闺蜜同游立减300元|普吉岛6日4晚自由行|入住梅尔加尔酒店(步行至海滩10分钟+赠送：接机服务)",
            ProductId = jsonObject.getString("ProductId");// "7158",
            Price = jsonObject.getString("Price");// "3199",
            SystemType = jsonObject.getString("SystemType");// "2",
            ProductType = jsonObject.optString("ProductType");// null,
            ImgSrc = jsonObject.getString("ImgSrc");// "http://media.china-sss.com/pics/1b43dfeb-e08b-4edf-864c-971b0ff50802_201502041546.jpg",
            ProductTypeName = jsonObject.getString("ProductTypeName");// "自由行",
            ProductTypeId = jsonObject.getString("ProductTypeId");// "2",
            BeginDate = jsonObject.optString("BeginDate");// null,
            EndDate = jsonObject.optString("EndDate");// null,
            DateTimeNow = jsonObject.getString("DateTimeNow");// "2015-03-22 20:28:37",
            BannerLink = jsonObject.optString("BannerLink");// null,
            ProductState = jsonObject.optString("ProductState");// null,
            ProductSubName = jsonObject.optString("ProductSubName");// null,
            Title = jsonObject.getString("Title");// "闺蜜同游立减300元|普吉岛6日4晚自由行|入住梅尔加尔酒店(步行至海滩10分钟+赠送：接机服务)",
            Destination = jsonObject.getString("Destination");// "普吉岛",
            Category = jsonObject.optString("Category");// null
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getProductName() {
        return ProductName;
    }

    public String getProductId() {
        return ProductId;
    }

    public String getPrice() {
        return Price;
    }

    public String getSystemType() {
        return SystemType;
    }

    public String getProductType() {
        return ProductType;
    }

    public String getImgSrc() {
        return ImgSrc;
    }

    public String getProductTypeName() {
        return ProductTypeName;
    }

    public String getProductTypeId() {
        return ProductTypeId;
    }

    public String getBeginDate() {
        return BeginDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public String getDateTimeNow() {
        return DateTimeNow;
    }

    public String getBannerLink() {
        return BannerLink;
    }

    public String getProductState() {
        return ProductState;
    }

    public String getProductSubName() {
        return ProductSubName;
    }

    public String getTitle() {
        return Title;
    }

    public String getDestination() {
        return Destination;
    }

    public String getCategory() {
        return Category;
    }
}
