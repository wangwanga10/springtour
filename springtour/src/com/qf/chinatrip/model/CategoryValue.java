package com.qf.chinatrip.model;

import org.json.JSONObject;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/25.
 * 15:10
 */
public class CategoryValue {
    private String Link;
    private String Title;

    private String ProductId;// "11348",
    private String SystemType;// "2",
    private String ProductTypeId;// "7"

    public void parseJson(JSONObject jsonObject) {
        Link = jsonObject.optString("Link");// "http://pages.springtour.com/Special/sj050227/index.html?app=1",
        Title = jsonObject.optString("Title");// ""
        ProductId = jsonObject.optString("ProductId");// "11348",
        SystemType = jsonObject.optString("SystemType");// "2",
        ProductTypeId = jsonObject.optString("ProductTypeId");// "7"
    }

    public String getLink() {
        return Link;
    }

    public String getTitle() {
        return Title;
    }

    public String getProductId() {
        return ProductId;
    }

    public String getSystemType() {
        return SystemType;
    }

    public String getProductTypeId() {
        return ProductTypeId;
    }
}
