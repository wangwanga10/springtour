package com.qf.chinatrip.model;

import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Fcy on 2015/3/23.
 */
public class ProductDetails implements Serializable {
    private String Title;//超值2晚●南通绿洲国际假日酒店3天2晚自由行●（含豪华自助早餐、入住期间大堂西点畅吃）
    private String ProductTypeName;//自由行 // 类别
    private String StartPrice;//218 // 新的价格
    private String DepartCity;//上海
    private String ImgSrc;//http://media.china-sss.com/pics/d583f245-1d20-48b8-b6b8-e71bf3fbfe23_201409121051_300_210.jpg
    // 产品经理推荐内容，有的是人物头像
    private String Recommend;//★狼山、濠河、张謇，如此一山一水一人构成了“中国近代第一城”南通。\n★酒店大堂的免费甜点，酒店的自助早餐已成为入住酒店的必选理由。\n★更有超值的自助餐厅、蒙古包餐厅、宴会厅供您选择。\n★建议晚上徒步夜游濠河，夜景特棒，别有一番滋味。\n★网上显示为单人价，此套餐需2人起订，1.2米以下儿童免费。\n
    private LinkedHashMap<String, String> RouteScheduleDict;
    private String RouteNo;//11348
    private String RouteId;//11348
    private String SystemType;//2
    private List<String> Range;//"2015-03-28","2015-04-11"
    private String ProductTypeId;//7
    private String ProductActivityLimit;//1
    private String TravelDays;//3 // 行程（3日）
    private boolean HasCollected; // false, // 是否收藏
    private int CollectedId; // 0
    //包含 key：Description 和 Pic
    private List<LinkedHashMap<String, String>> ProductPics; // 图库
    private List<LinkedHashMap<String, String>> SubDict; // 基本信息
    private String RecommendManagerImgUrl;//http://media.china-sss.com/pics/4696e6a5-0b20-4436-b39c-64b30d7deb57_201409180021_300_210.jpg
    private String CommentCount;//0 // 点评个数
    private String OrignPrice;//218 // 老价格
    private String SpecialOffers;// // “优惠”，如果此内容为""，那么不显示该模块
    private String Destination;//南通
    private String Travel;//3天2夜
    private String Schedule;//详见日历

    public void parseJson(JSONObject json) {
        try {
            Title = json.getString("Title");
            ProductTypeName = json.getString("ProductTypeName");
            StartPrice = json.getString("StartPrice");
            DepartCity = json.getString("DepartCity");
            ImgSrc = json.getString("ImgSrc");

            Recommend = json.getString("Recommend");
            RouteScheduleDict = JSON.parseObject(
                    json.getJSONObject("RouteScheduleDict").toString(),
                    new TypeReference<LinkedHashMap<String, String>>() {
                    });

            RouteNo = json.getString("RouteNo");
            RouteId = json.getString("RouteId");
            SystemType = json.getString("SystemType");

            Range = JSON.parseObject(
                    json.getJSONArray("Range").toString(),
                    new TypeReference<List<String>>() {
                    });
            ProductTypeId = json.getString("ProductTypeId");
            ProductActivityLimit = json.getString("ProductActivityLimit");
            TravelDays = json.getString("TravelDays");
            HasCollected = json.getBoolean("HasCollected");
            CollectedId = json.getInt("CollectedId");

            RecommendManagerImgUrl = json.getString("RecommendManagerImgUrl");
            CommentCount = json.getString("CommentCount");
            OrignPrice = json.getString("OrignPrice");
            SpecialOffers = json.getString("SpecialOffers");
            Destination = json.getString("Destination");
            Travel = json.getString("Travel");
            Schedule = json.getString("Schedule");

            ProductPics = JSON.parseObject(
                    json.getJSONArray("ProductPics").toString(),
                    new TypeReference<List<LinkedHashMap<String, String>>>() {
                    });
            SubDict = JSON.parseObject(
                    json.getJSONArray("SubDict").toString(),
                    new TypeReference<List<LinkedHashMap<String, String>>>() {
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getProductTypeName() {
        return ProductTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        ProductTypeName = productTypeName;
    }

    public String getStartPrice() {
        return StartPrice;
    }

    public void setStartPrice(String startPrice) {
        StartPrice = startPrice;
    }

    public String getDepartCity() {
        return DepartCity;
    }

    public void setDepartCity(String departCity) {
        DepartCity = departCity;
    }

    public String getImgSrc() {
        return ImgSrc;
    }

    public void setImgSrc(String imgSrc) {
        ImgSrc = imgSrc;
    }

    public String getRecommend() {
        return Recommend;
    }

    public void setRecommend(String recommend) {
        Recommend = recommend;
    }

    public LinkedHashMap<String, String> getRouteScheduleDict() {
        return RouteScheduleDict;
    }

    public void setRouteScheduleDict(LinkedHashMap<String, String> routeScheduleDict) {
        RouteScheduleDict = routeScheduleDict;
    }

    public String getRouteNo() {
        return RouteNo;
    }

    public void setRouteNo(String routeNo) {
        RouteNo = routeNo;
    }

    public String getRouteId() {
        return RouteId;
    }

    public void setRouteId(String routeId) {
        RouteId = routeId;
    }

    public String getSystemType() {
        return SystemType;
    }

    public void setSystemType(String systemType) {
        SystemType = systemType;
    }

    public List<String> getRange() {
        return Range;
    }

    public void setRange(List<String> range) {
        Range = range;
    }

    public String getProductTypeId() {
        return ProductTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        ProductTypeId = productTypeId;
    }

    public String getProductActivityLimit() {
        return ProductActivityLimit;
    }

    public void setProductActivityLimit(String productActivityLimit) {
        ProductActivityLimit = productActivityLimit;
    }

    public String getTravelDays() {
        return TravelDays;
    }

    public void setTravelDays(String travelDays) {
        TravelDays = travelDays;
    }

    public boolean isHasCollected() {
        return HasCollected;
    }

    public void setHasCollected(boolean hasCollected) {
        HasCollected = hasCollected;
    }

    public int getCollectedId() {
        return CollectedId;
    }

    public void setCollectedId(int collectedId) {
        CollectedId = collectedId;
    }

    public List<LinkedHashMap<String, String>> getProductPics() {
        return ProductPics;
    }

    public void setProductPics(List<LinkedHashMap<String, String>> productPics) {
        ProductPics = productPics;
    }

    public List<LinkedHashMap<String, String>> getSubDict() {
        return SubDict;
    }

    public void setSubDict(List<LinkedHashMap<String, String>> subDict) {
        SubDict = subDict;
    }

    public String getRecommendManagerImgUrl() {
        return RecommendManagerImgUrl;
    }

    public void setRecommendManagerImgUrl(String recommendManagerImgUrl) {
        RecommendManagerImgUrl = recommendManagerImgUrl;
    }

    public String getCommentCount() {
        return CommentCount;
    }

    public void setCommentCount(String commentCount) {
        CommentCount = commentCount;
    }

    public String getOrignPrice() {
        return OrignPrice;
    }

    public void setOrignPrice(String orignPrice) {
        OrignPrice = orignPrice;
    }

    public String getSpecialOffers() {
        return SpecialOffers;
    }

    public void setSpecialOffers(String specialOffers) {
        SpecialOffers = specialOffers;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        Destination = destination;
    }

    public String getTravel() {
        return Travel;
    }

    public void setTravel(String travel) {
        Travel = travel;
    }

    public String getSchedule() {
        return Schedule;
    }

    public void setSchedule(String schedule) {
        Schedule = schedule;
    }

    @Override
    public String toString() {
        return "ProductDetails{" +
                "Schedule='" + Schedule + '\'' +
                ", Title='" + Title + '\'' +
                ", ProductTypeName='" + ProductTypeName + '\'' +
                ", StartPrice='" + StartPrice + '\'' +
                ", DepartCity='" + DepartCity + '\'' +
                ", ImgSrc='" + ImgSrc + '\'' +
                ", Recommend='" + Recommend + '\'' +
                ", RouteScheduleDict=" + RouteScheduleDict +
                ", RouteNo='" + RouteNo + '\'' +
                ", RouteId='" + RouteId + '\'' +
                ", SystemType='" + SystemType + '\'' +
                ", Range=" + Range +
                ", ProductTypeId='" + ProductTypeId + '\'' +
                ", ProductActivityLimit='" + ProductActivityLimit + '\'' +
                ", TravelDays='" + TravelDays + '\'' +
                ", HasCollected=" + HasCollected +
                ", CollectedId=" + CollectedId +
                ", ProductPics=" + ProductPics +
                ", SubDict=" + SubDict +
                ", RecommendManagerImgUrl='" + RecommendManagerImgUrl + '\'' +
                ", CommentCount='" + CommentCount + '\'' +
                ", OrignPrice='" + OrignPrice + '\'' +
                ", SpecialOffers='" + SpecialOffers + '\'' +
                ", Destination='" + Destination + '\'' +
                ", Travel='" + Travel + '\'' +
                '}';
    }
}
