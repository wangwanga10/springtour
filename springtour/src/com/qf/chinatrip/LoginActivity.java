package com.qf.chinatrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;

import java.util.HashMap;

/**
 * Created by Fcy on 2015/3/24.
 */
public class LoginActivity extends Activity implements View.OnClickListener, PlatformActionListener {

    private ImageView clearAccount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
    }

    private void initView() {
        findViewById(R.id.login_title_bar_close).setOnClickListener(this);
        findViewById(R.id.login_title_bar_register).setOnClickListener(this);
        EditText inputAccount = (EditText) findViewById(R.id.login_account_input);
        // 监听文字内容变化
        inputAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 如果文字不为空，且按钮不可见，则设置按钮可见
                if (!TextUtils.isEmpty(s) && clearAccount.getVisibility() != View.VISIBLE) {
                    clearAccount.setVisibility(View.VISIBLE);
                } else {
                    clearAccount.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        EditText inputPw = (EditText) findViewById(R.id.login_password_input);
        inputPw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        clearAccount = (ImageView) findViewById(R.id.login_account_clear);
        clearAccount.setOnClickListener(this);
        ImageView clearPassword = (ImageView) findViewById(R.id.login_password_clear);
        clearPassword.setOnClickListener(this);

        // 第三方登录
        findViewById(R.id.login_other_qq).setOnClickListener(this);
        findViewById(R.id.login_other_weixin).setOnClickListener(this);
        findViewById(R.id.login_other_xlwb).setOnClickListener(this);

    }

    /**
     * @param plat 希望用那个平台登录，这个地方就传递那个平台的对象
     */
    private void authorize(Platform plat) {
        if (plat == null) {
            // 如果没有指定平台，那么现在强制选择新浪微博
//            plat = new SinaWeibo(this);
            return;
        }

        //判断指定平台是否已经完成授权
        // 没有授权的时候，进行授权操作 最终会执行 showUser() 这个方法
        // 作用就是授权并且获取用户信息
//        if (plat.isValid()) { // 已完成授权
//            String userId = plat.getDb().getUserId();
//            if (userId != null) {
//                // 用于发送注册/登陆成功的消息
////                UIHandler.sendEmptyMessage(1, this);
//                // login(plat.getName(), userId, null);
//
//                Log.i(">>已授权", plat.getName());
//                Log.i(">>已授权", userId);
//
//                return;
//            }
//        }
        // 用于检测用户在登录时操作的状态。
        // 当用户授权成功，会进行回调，回调内部会传递一些用户的信息
        plat.setPlatformActionListener(this);
        // true不使用SSO授权，false使用SSO授权
        plat.SSOSetting(true);
        //获取用户资料
        plat.showUser(null);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.login_title_bar_close: // 关闭
                break;
            case R.id.login_title_bar_register: // 注册
                break;
            case R.id.login_account_clear:// 清除账号
                break;
            case R.id.login_password_clear:// 清除密码
                break;
            case R.id.login_btn_login: // 登录
                break;
            case R.id.login_forget_password: // 忘记密码
                break;
            case R.id.login_other_qq: // qq登录
                // 这个方法必须调用，初始化ShareSDK
                ShareSDK.initSDK(this);
                authorize(new QQ(this));
                break;
            case R.id.login_other_weixin: // 微信登录
                break;
            case R.id.login_other_xlwb: // 新浪微博
                break;

        }

        if (intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
        // 获取哪一个平台的登录授权信息
        String name = platform.getName();
        // 获取特定平台下面的用户数据库
        PlatformDb db = platform.getDb();
        // 授权的用户名称
        String userName = db.getUserName();
        Log.i(">>授权成功", name + " -> " + userName);
        String userId = db.getUserId();


    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {
        Log.i(">>", "授权失败");
    }

    @Override
    public void onCancel(Platform platform, int i) {

    }

}