package com.qf.chinatrip.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * 自定义ImageView实现指定ImageView的宽度，高度根据图片的宽高自动缩放。
 * 重写onMeasure（）方法，通过计算图片的宽高，与ImageView的宽度，然后等比算出需求高度
 */
public class WidthFixedImageView extends ImageView {
    public WidthFixedImageView(Context context) {
        super(context);
    }

    public WidthFixedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WidthFixedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            // TODO 宽度为match或固定值，根据图片的宽高等比缩放设置ImageView的 高度
            // 计算ImageView的宽度
            int width = MeasureSpec.getSize(widthMeasureSpec);
            // // 根据Drawable的宽度和高度，算出等比缩放后的ImageView的高度
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (intrinsicWidth != 0) {
                int height = width * drawable.getIntrinsicHeight() / intrinsicWidth;
                // 通过计算出来的高度并设置ImageView的高度类型为 EXACTLY 的，来设置 heightMeasureSpec
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
            }
        }

        // TODO 设置需要设置等比缩放后的宽高及类型
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
