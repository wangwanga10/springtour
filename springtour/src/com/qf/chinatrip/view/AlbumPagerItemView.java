package com.qf.chinatrip.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.lidroid.xutils.BitmapUtils;
import com.qf.chinatrip.R;
import com.qf.chinatrip.utils.BitmapUtilsHelper;

/**
 * Created by Fcy on 2015/3/23.
 * 相册的ItemView,自定义View.方便复用
 */
public class AlbumPagerItemView extends FrameLayout {
    private WidthFixedImageView imageView;
    private String imgUrl;
    private Bitmap bitmap;

    public AlbumPagerItemView(Context context) {
        super(context);
        initView();
    }

    public AlbumPagerItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    /**
     * 初始化view
     */
    public void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_album, null);
        imageView = (WidthFixedImageView) view.findViewById(R.id.album_img);
        addView(view);
    }

    /**
     * 填充数据，供外部调用
     *
     * @param imgUrl 图片地址（这里只显示图片，所以穿个图片地址即可）
     */
    public void setData(String imgUrl) {
        this.imgUrl = imgUrl;
        // TODO 下载图片，并显示
        BitmapUtils bitmapUtils = BitmapUtilsHelper.initUtils(getContext());
        bitmapUtils.display(imageView, imgUrl);
    }

    /**
     * 这里内存回收.外部调用.
     */
    public void recycle() {
        imageView.setImageBitmap(null);
        if ((bitmap == null) || (bitmap.isRecycled()))
            return;
        bitmap.recycle();
        bitmap = null;
    }

    /**
     * 重新加载.外部调用.
     */
    public void reload() {
        if (!TextUtils.isEmpty(imgUrl)) {
            setData(imgUrl);
        }
    }

}
