package com.qf.chinatrip.utils;

/**
 * Created by Fcy on 2015/3/23.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.qf.chinatrip.R;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 通用的工具
 */
public final class CommonUtils {
    public static void toast(Context context, String toastMsg) {
        Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取屏幕的宽度
     */
    public static int getWindowsWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * @param url 指定的任意字符串
     * @return 指定字符串通过md5运算并转化为字符串，如果url为null或长度为0，则返回null
     */
    public static String urlMDToString(String url) {
        String result = null;
        if (url != null && url.length() > 0) {
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(url.getBytes());
                byte[] tar = md5.digest();
                StringBuilder sb = new StringBuilder("");
                for (byte b : tar) {
                    int h = (b >> 4) & 15;
                    int l = b & 15;
                    // 因为4位二进制数最大为1111，即15
                    sb.append(Integer.toHexString(h)).append(
                            Integer.toHexString(l));
                }
                result = sb.toString();

            } catch (NoSuchAlgorithmException e) {
                result = String.valueOf(url.hashCode());
                e.printStackTrace();
            }
        }

        return result;
    }

    /**
     * 获取一个通用的包含文字和progressbar的Dialog，如果登录提醒，检查更新等
     * 不能更改布局 R.layout.dialog_progress
     *
     * @param context
     * @param content 提示文本
     * @return
     */
    public static Dialog getProgressDialog(Context context, String content) {
        Dialog dialog = new Dialog(context, R.style.myDialog);
        dialog.setContentView(R.layout.dialog_progress);
        TextView tvProgressContent = (TextView) dialog.findViewById(R.id.progress_content);
        tvProgressContent.setText(content);
        return dialog;
    }

    /**
     * 在卸载应用后，会自动删除该文件夹
     *
     * @param context
     * @param uniqueName 保存文件夹名称
     * @return
     */
    public static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath = null;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)
                || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else { // 内存卡不存在,获取应用地址
            cachePath = context.getCacheDir().getPath();
        }
        File cacheFile = new File(cachePath + File.separator + uniqueName);
        // 如果文件夹不存在，则创建
        if (!cacheFile.exists()) {
            cacheFile.mkdirs();
        }
        return cacheFile;
    }

    /**
     * 检验是否为手机号
     *
     * @param phoneNum
     * @return
     */
    public static boolean isMobile(String phoneNum) {
        Pattern pattern = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$");
        Matcher matcher = pattern.matcher(phoneNum);
        return matcher.matches();
    }

    /**
     * [获取应用程序版本名称信息]
     *
     * @param context
     * @return 当前应用的版本号
     */
    public static int getVersionCode(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }
}
