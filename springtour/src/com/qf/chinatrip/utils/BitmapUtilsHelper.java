package com.qf.chinatrip.utils;

import android.content.Context;
import android.util.Log;
import com.lidroid.xutils.BitmapUtils;

/**
 * Created by Fcy on 2015/3/23.
 */
public class BitmapUtilsHelper {
    public static BitmapUtils initUtils(Context context) {
        return initUtils(context, -1);
    }

    /**
     * @param context
     * @param defaultImgResId 默认加载中和加载失败显示的图片资源id
     * @return
     */
    public static BitmapUtils initUtils(Context context, int defaultImgResId) {
        String diskCachePath = CommonUtils.getDiskCacheDir(context, "Image").getAbsolutePath();
        // 设置文件缓存 diskCacheSize 为 100M
        BitmapUtils utils = new BitmapUtils(context, diskCachePath, 1 / 8, 1024 * 1024 * 100);
        //磁盘缓存的过期时间
        utils.configDefaultCacheExpiry(1000 * 60 * 60 * 24 * 7); // 7天后过期
        //联接超时
        utils.configDefaultConnectTimeout(3000);
        //读取超时
        utils.configDefaultReadTimeout(3000);
        if (defaultImgResId != -1) {
            utils.configDefaultLoadingImage(context.getResources().getDrawable(defaultImgResId));
            utils.configDefaultLoadFailedImage(context.getResources().getDrawable(defaultImgResId));
        }
        return utils;
    }
}
