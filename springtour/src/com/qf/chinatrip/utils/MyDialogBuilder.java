package com.qf.chinatrip.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.qf.chinatrip.R;

/**
 * 自定义回调接口处理点击事件:
 * 1.定义一个内部接口OnClickListener，接口中定义回调方法。
 * 2.定义一个接口的成员变量
 * 3.定义设置自定义监听点击处理方法，方法参数接收一个内部接口实例，用于初始化成员OnClickListener
 * 4.监听点击事件，点击时回调接口的onClick方法，进行处理
 */
public class MyDialogBuilder implements View.OnClickListener {
    private Dialog dialog;
    private Button btnCancel, btnConfirm;
    private TextView showTitle, showContent;

    private OnClickListener mNegativeButtonListener;
    private OnClickListener mPositiveButtonListener;

    /**
     * @param context 上下文
     * @param title   对话框标题
     * @param content 对话框内容
     */
    public MyDialogBuilder(Context context, String title, String content) {
        dialog = new Dialog(context, R.style.myDialog);

        // TODO 为 Dialog 设置视图
        View view = LayoutInflater.from(context).inflate(R.layout.common_min_height_dialog, null);
        dialog.setContentView(view);

        btnCancel = (Button) view.findViewById(R.id.common_dialog_btn_cancel);
        btnConfirm = (Button) view.findViewById(R.id.common_dialog_btn_confirm);

        // TODO 监听取消和确定按钮的点击事件
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);


        // TODO 为 Dialog 设置标题和内容
        showTitle = (TextView) view.findViewById(R.id.common_dialog_title);
        showContent = (TextView) view
                .findViewById(R.id.common_dialog_content_text);

        showTitle.setText(title);
        showContent.setText(content);

    }

    /**
     * 创建Dialog
     *
     * @return 根据指定条件创建的Dialog
     */
    public Dialog create() {
        if (dialog == null) {
            throw new IllegalStateException("必须初始化Dialog");
        }
        return dialog;
    }

    /**
     * 为Dialog设置取消按钮
     *
     * @param cancelText 取消按钮文字
     * @param callBack   回调接口
     * @return
     */
    public MyDialogBuilder setNegativeButton(String cancelText, OnClickListener callBack) {
        btnCancel.setText(cancelText);
        mNegativeButtonListener = callBack;
        return this;
    }

    /**
     * 为Dialog设置确定按钮
     *
     * @param confirmText 确定按钮文字
     * @param callBack    回调接口
     * @return
     */
    public MyDialogBuilder setPositiveButton(String confirmText, OnClickListener callBack) {
        btnConfirm.setText(confirmText);
        mPositiveButtonListener = callBack;
        return this;
    }

    /**
     * 监听取消、确定按钮的点击事件，当被点击时，回调
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        // 关闭Dialog
        dialog.dismiss();

        switch (v.getId()) {
            case R.id.common_dialog_btn_cancel:
                if (mNegativeButtonListener != null) {
                    mNegativeButtonListener.onClick();
                }
                break;
            case R.id.common_dialog_btn_confirm:
                if (mPositiveButtonListener != null) {
                    mPositiveButtonListener.onClick();
                }
                break;
        }
    }

    /**
     * 回调接口，用于处理点击取消、确定按钮的操作
     */
    public interface OnClickListener {
        public void onClick();
    }
}
