package com.qf.chinatrip;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import com.qf.chinatrip.adapter.CommonFragmentAdapter;
import com.qf.chinatrip.fragment.TutorialImageFragment;
import com.qf.chinatrip.fragment.TutorialJumpFragment;

import java.util.LinkedList;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 18:11
 */
public class TutorialActivity extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        SharedPreferences sharedPreferences = getSharedPreferences(Constans.SP_APP_DATA, MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(Constans.SP_KEY_TUTORIAL_SHOWN, true).commit();

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_tutorial);

        LinkedList<Fragment> fragments = new LinkedList<Fragment>();
        //TODO 添加教程界面

        TutorialImageFragment imageFragment = new TutorialImageFragment();
        Bundle args = new Bundle();
        args.putInt(Constans.ARG_TUTORIAL_IMAGE_LAYOUT_ID, R.layout.fragment_tutorial_image1);
        imageFragment.setArguments(args);
        fragments.add(imageFragment);

        imageFragment = new TutorialImageFragment();
        args = new Bundle();
        args.putInt(Constans.ARG_TUTORIAL_IMAGE_LAYOUT_ID, R.layout.fragment_tutorial_image2);
        imageFragment.setArguments(args);
        fragments.add(imageFragment);

        imageFragment = new TutorialImageFragment();
        args = new Bundle();
        args.putInt(Constans.ARG_TUTORIAL_IMAGE_LAYOUT_ID, R.layout.fragment_tutorial_image3);
        imageFragment.setArguments(args);
        fragments.add(imageFragment);

        imageFragment = new TutorialImageFragment();
        args = new Bundle();
        args.putInt(Constans.ARG_TUTORIAL_IMAGE_LAYOUT_ID, R.layout.fragment_tutorial_image4);
        imageFragment.setArguments(args);
        fragments.add(imageFragment);

        fragments.add(new TutorialJumpFragment());

        CommonFragmentAdapter adapter = new CommonFragmentAdapter(
                getSupportFragmentManager(),
                fragments
        );

        viewPager.setAdapter(adapter);
    }
}