package com.qf.chinatrip;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 18:02
 */

/**
 * 常亮类
 */
public class Constans {

    /**
     * 整个应用程序 SharedPreference 的名称
     */
    public static final String SP_APP_DATA = "appData";

    /**
     * 保存教程是否显示的 SharedPreference 的 key
     */
    public static final String SP_KEY_TUTORIAL_SHOWN = "tutorial.shown";

    /**
     * 用于教程部分,对图片显示 Fragment 进行参数设置的,
     * 代表 Fragment 显示哪个布局
     */
    public static final String ARG_TUTORIAL_IMAGE_LAYOUT_ID = "layoutId";
    public static final String ADVERTISIMENT_IMAGE_URL = "Ad_Image_Url";
    public static final String HOME_URl = "http://m.springtour.com/home/appindex/AllPageReveal?orderFrom=5";
    public static final String KEY_INTENT_PRODUCT_ID = "ProductId";
}
