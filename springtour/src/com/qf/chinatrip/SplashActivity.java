package com.qf.chinatrip;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;

/**
 * Created by IDEA
 * User : SL
 * on  2015/3/23.
 * 17:18
 */

/**
 * 欢迎界面
 */
public class SplashActivity extends Activity implements Runnable {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);

        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1500);

            SharedPreferences sharedPreferences = getSharedPreferences(Constans.SP_APP_DATA, MODE_PRIVATE);

            boolean b = sharedPreferences.getBoolean(Constans.SP_KEY_TUTORIAL_SHOWN, false);

            if (b){
                startActivity(new Intent(this, MainActivity.class));
            }else {
                startActivity(new Intent(this, TutorialActivity.class));
            }

            finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}