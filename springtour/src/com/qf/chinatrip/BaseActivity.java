package com.qf.chinatrip;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.qf.chinatrip.utils.MyDialogBuilder;

/**
 * Created by Fcy on 2015/3/20.
 * 公用的标题栏为TitleBar的Activity
 */
public class BaseActivity extends FragmentActivity implements View.OnClickListener {
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new MyDialogBuilder(
                this, "联系我们", getResources().getString(R.string.call_me_content))
                .setPositiveButton("拨打", new MyDialogBuilder.OnClickListener() {
                    @Override
                    public void onClick() {
                        // TODO 拨打电话 400-820-6222

                    }
                }).create();

    }

    @Override
    public void setTitle(CharSequence title) {
//        super.setTitle(title); // 默认修改的是titleBar的标题
        if (title == null) {
            throw new IllegalStateException("请设置标题");
        } else {
            TextView textTitle = (TextView) findViewById(R.id.title_bar_ibt_title);
            textTitle.setText(title);
        }

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        // 处理点击事件
        findViewById(R.id.title_bar_ibt_back).setOnClickListener(this);
        findViewById(R.id.title_bar_ibt_phone).setOnClickListener(this);
        findViewById(R.id.title_bar_ibt_search).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_bar_ibt_back: // 返回键
                finish();
                break;
            case R.id.title_bar_ibt_phone: // 电话
                Log.i(">>","拨打电话");
                // TODO 弹出打电话的对话框
                dialog.show();
                break;
            case R.id.title_bar_ibt_search: // 搜索
                // TODO 跳向 SearchActivity
                break;
        }
    }
}